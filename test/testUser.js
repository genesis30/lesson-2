'use strict';

// Dependencies
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');


// Let chai use HTTP Requests
chai.use(chaihttp);

var should = chai.should();
var server = require('../server');


describe('First test',
  function() {
    it('Test that google works', function(done) {
      chai.request('https://google.com/')
        .get('/')
        .end(
          function(err, res) {
            res.should.have.status(200);
            done();
          }
        )
    })
  }
)


describe('API Alive',
  function() {
    it('Test that the API is working!', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            res.should.have.status(200);
            res.body.msg.should.be.eql('Hello man!');
            done();
          }
        )
    })
  }
)


describe('User Test',
  function() {
    it('Test user list has data.', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            res.should.have.status(200);

            for(let user of res.body.users) {
              user.should.have.property('id');
              user.should.have.property('email');
              user.should.have.property('password');
              user.should.have.property('first_name');
              user.should.have.property('last_name');
            }

            done();
          }
        )
    })
  }
)
