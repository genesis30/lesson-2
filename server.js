'use strict';

/**
 * Module Dependencies
 */
const config = require('./config');
const express = require('express');

/**
 * Init the APP
 */
const app = express();
// Body parser
app.use(express.json());

// Routes
var routes = require('./api/routes/apiRoutes');
routes(app); //register the routes

// 404 Page
app.use(function(req, res) {
	res.status(404).send({url: req.originalUrl + ' not found'});
});

/**
 * Run the APP
 */
app.listen(config.port);
console.log('RESTful API server started on port: ' + config.port);
