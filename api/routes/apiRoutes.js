'use strict';

module.exports = function(app) {

  var miscRoutes = require('./miscRoutes');
  miscRoutes(app);

  var userRoutes = require('./userRoutes');
  userRoutes(app);

  var authRoutes = require('./authRoutes');
  authRoutes(app);

};
