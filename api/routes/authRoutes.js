'use strict';

module.exports = function(app) {
  var auth = require('../controllers/authController');

  // Auth routes
  app.route('/apitechu/v1/login')
    .post(auth.login);

  app.route('/apitechu/v1/logout')
    .post(auth.logout);
};
