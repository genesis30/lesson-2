'use strict';

module.exports = function(app) {
  var misc = require('../controllers/miscController');

  // Hello routes
  app.route('/apitechu/v1/hello')
    .get(misc.greet);
  // Monster routes
  app.route('/apitechu/v1/monster/:p1/:p2')
    .get(misc.all);
};
