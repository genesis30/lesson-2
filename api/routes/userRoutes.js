'use strict';

module.exports = function(app) {
  var users = require('../controllers/userController');

  // User routes
  app.route('/apitechu/v1/users')
    .get(users.list_users)
    .post(users.create_user);

  app.route('/apitechu/v1/users/:userId')
    .delete(users.delete_user);
};
