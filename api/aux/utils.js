'use strict';


exports.writeUserDataToFile = function(users) {
  const fs = require('fs');

  fs.writeFile('./files/users.json', JSON.stringify(users), 'utf-8',
    function(err) {
      if (err)
        console.log(err);
      console.log('Data written to file.')
    }
  );
};


exports.deleteUser = function(index, users) {
  users.splice(index, 1);
  writeUserDataToFile(users);
};



/**
 * For iteration
 */
function iterate_for(users, id) {
  for (var i = 0; i < users.length; i++) {
    let user = users[i];
    if (user.id == id) {
      return i;
    }
  }
}


/**
 * For-IN iteration
 */
function iterate_for_in(users, id) {
  for (let user in users) {
    if (user.id == id) {
      return users.indexOf(user);
    }
  }
}


/**
 * For-OF iteration
 */
function iterate_for_of(users, id) {
  for (let user of users) {
    if (user.id == id) {
      return users.indexOf(user);
    }
  }
}


/**
 * For-Each iteration
 */
function iterate_for_each(users, id) {
  var foundUser;

  users.forEach(function(user) {
    if (user.id == id) {
      foundUser = user;
    }
  });

  return users.indexOf(foundUser);
}


/**
 * Find Index iteration
 */
exports.iterate_find_index = function(users, id) {
  return users.findIndex(user => user.id == id);
};
