'use strict';

/**
 * Greets a user
 * @name GET /apitechu/v1/hello
 * @function
 */
exports.greet = function(req, res) {
  console.log('GET /apitechu/v1/hello');
  // Send the response
  res.send({'msg': 'Hello man!'});
};

/**
 * Testing all parameters.
 * @name POST /apitechu/v1/monster/:p1/:p2
 * @function
 */
exports.all = function(req, res) {
  console.log('POST /apitechu/v1/monster/:p1/:p2');
  console.log('Parameters: ' + JSON.stringify(req.params));
  console.log('Query String: ' + JSON.stringify(req.query));
  console.log('Headers: ' + JSON.stringify(req.headers));
  console.log('Body: ' + JSON.stringify(req.body));
  // Send response
  res.send({'msg':'Done.'});
};
