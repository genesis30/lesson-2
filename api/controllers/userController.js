'use strict';

// Utility functions
const utils = require('../aux/utils');

/**
 * List all users.
 * @name GET /apitechu/v1/users
 * @function
 * @param {number} req.query.$top Filter to limit the number of users to return
 * @param {boolean} req.query.$count Filter to return the count of total users
 */
exports.list_users = function(req, res) {
  console.log("GET /apitechu/v1/users");
  // Load users
  var result = {};
  var users = require('../../files/users.json');
  // Check for filters
  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) :
    users;
  // Send response
  res.send(result);
};

/**
 * Create a user
 * @name POST /apitechu/v1/users
 * @function
 * @param {string} req.body.first_name First name of the user
 * @param {string} req.body.last_name Last name of the user
 * @param {string} req.body.email Email of the user
 */
exports.create_user = function(req, res) {
  console.log('POST /apitechu/v1/users');
  // Create the new user
  var newUser = {
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email
  };
  // Load users
  var users = require('../../files/users.json');
  // Push the user to the list and write it to disk
  users.push(newUser);
  utils.writerUserDataToFile(users);
  // Send response
  res.send({'msg': 'User created!'});
};

/**
 * Delete a user
 * @name DELETE /apitechu/v1/users/:id
 * @function
 * @param {number} req.params.id ID of the user to delete
 */
exports.delete_user = function(req, res) {
  console.log('DELETE /apitechu/v1/users/:id');
  console.log('Sent ID is: ' + req.params.id);
  // Load users and delete the required user from the list
  var users = require('../../files/users.json');
  var index = utils.iterate_find_index(users, req.params.id);
  utils.deleteUser(index, users);
  // Send response
  res.send({'msg': 'User deleted.'});
};
