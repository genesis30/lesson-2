'use strict';

// Utility functions
const utils = require('../aux/utils');

/**
 * Logs in a user into the system.
 * @name POST /apitechu/v1/login
 * @function
 * @param {string} req.body.email Email of the user
 * @param {string} req.body.password Password of the user
 */
exports.login = function(req, res) {
  if (!req.body || !req.body.email || !req.body.password) {
    console.log('Missing data...');
    res.status(400).send({'msg': 'Bad Request'});
  } else {
    // This is not very classy, but leave it as it is for the moment
    // Load users and look for the user in the list
    var users = require('../../files/users.json');
    var index = users.findIndex(user => user.email == req.body.email);
    // If the user is found
    if (index >= 0) {
      // Matching passwords
      if (users[index].password == req.body.password) {
        // Update user and save the change
        console.log('Passwords matched');
        users[index].logged = true;
        utils.writeUserDataToFile(users);
        // Send response
        res.send({
          'msg': 'User logged in.',
          'user_id': users[index].id
        });
      } else {
        console.log('Pass did not match...');
        // Send response
        res.status(403).send({'msg': 'Not allowed'});
      }
    } else {
      console.log('User not found...');
      // Send response
      res.status(403).send({'msg': 'Not allowed'});
    }
  }
};

/**
 * Logs a user out of the system.
 * @name POST /apitechu/v1/logout
 * @function
 * @param {string} req.body.id ID of the user to log out
 */
exports.logout = function(req, res) {
  if (!req.body || !req.body.id) {
    console.log('Missing data...');
    res.status(400).send({'msg': 'Bad Request'});
  } else {
    // This is not very classy, but leave it as it is for the moment
    // Load users and look for the user in the list
    var users = require('../../files/users.json');
    var index = users.findIndex(user => user.id == req.body.id);
    // If the user is found and the user is logged
    if (index >= 0 && users[index].logged) {
      // Update user and save the change
      console.log('Logging out user.');
      delete users[index].logged;
      utils.writeUserDataToFile(users);
      // Send the response
      res.send({
        'msg': 'User logged out.',
        'user_id': users[index].id
      });
    } else {
      console.log('User not found or user not logged in.');
      // Send the response
      res.status(403).send({'msg': 'Incorrect logout'});
    }
  }
};
